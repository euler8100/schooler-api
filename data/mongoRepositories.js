const mongoose = require('mongoose');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');
const mongoRepoClasses = [
  require('./userMongoRepo').UserMongoRepo,
  require('./messageMongoRepo').MessageMongoRepo,
];

class MongoRepositories {
  static async init() {
    const connectionConfig = DBUtils.getDBConnectionOptions();
    const connection = await mongoose
      .connect(`${process.env.DATABASE_URL}`, connectionConfig)
      .catch((error) => Promise.reject(new Error(error)));
    mongoose.set('debug', true);
    return generateMongoRepoInstances(connection);
  }
}

function generateMongoRepoInstances(mongoConnection) {
  return mongoRepoClasses.reduce((mongoRepositories, mongoRepoClass) => {
    const mongoRepoInstance = new mongoRepoClass(mongoConnection);
    const repoName = lowerCaseFirsLetter(mongoRepoInstance.constructor.name);
    mongoRepositories[repoName] = mongoRepoInstance;
    return mongoRepositories;
  }, {});
}

function lowerCaseFirsLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

exports.MongoRepositories = MongoRepositories;
