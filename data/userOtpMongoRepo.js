const { IRepository } = require('./iRepository');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');

class UserOtpMongoRepo extends IRepository {
  constructor(connection) {
    super();
    if (!this.connection) {
      this.connection = connection;
    }
    this.dataModel = {};
  }

  async insert(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    await dataModel.insertMany(dataToSave);
  }

  async findOne(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { otp: data.getOtp(), phoneNumber: data.getPhoneNumber() };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async deleteOne(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { otp: data.getOtp(), phoneNumber: data.getPhoneNumber() };
    return dataModel.deleteOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async deleteMany(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { phoneNumber: data.getPhoneNumber() };
    return dataModel.deleteMany(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  getDataModelInstance(data) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataName = sampleData.constructor.name;
    if (!this.dataModel[dataName]) {
      this.dataModel[dataName] = DBUtils.buildModelFrom(data, this.connection);
    }

    return this.dataModel[dataName];
  }
}

exports.UserOtpMongoRepo = UserOtpMongoRepo;
