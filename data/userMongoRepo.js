const { IRepository } = require('./iRepository');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');

class UserMongoRepo extends IRepository {
  constructor(connection) {
    super();
    if (!this.connection) {
      this.connection = connection;
    }
    this.dataModel = {};
  }

  async insert(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    await dataModel.insertMany(dataToSave);
  }

  async update(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    const searchQuery = { uuid: data.getUuid() };
    await dataModel.updateOne(searchQuery, dataToSave);
  }

  async get(data) {
    const dataModel = this.getDataModelInstance(data);
    return dataModel.find({}, { _id: 0, __v: 0 }).sort({ fullName: 1 }).lean();
  }

  async delete(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { uuid: data.getUuid() };
    await dataModel.deleteOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async findOne(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { $or: [{ uuid: data.getUuid() }, { pseudo: data.getPseudo() }] };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  getDataModelInstance(data) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataName = sampleData.constructor.name;
    if (!this.dataModel[dataName]) {
      this.dataModel[dataName] = DBUtils.buildModelFrom(data, this.connection);
    }

    return this.dataModel[dataName];
  }
}

exports.UserMongoRepo = UserMongoRepo;
