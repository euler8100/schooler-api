const { IRepository } = require('./iRepository');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');

class TransactionMongoRepo extends IRepository {
  constructor(connection) {
    super();
    if (!this.connection) {
      this.connection = connection;
    }
    this.dataModel = {};
  }

  async insert(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    await dataModel.insertMany(dataToSave);
  }

  async update(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    const searchQuery = { uuid: data.getUuid() };
    return dataModel.updateOne(searchQuery, dataToSave);
  }

  async findOne(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { uuid: data.getUuid() };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async findByPaymentReference(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { paymentApiReference: data.getPaymentApiReference() };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async find(data, accountsPhoneNumbers) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = {
      $or: [
        { senderPhoneNumber: { $in: accountsPhoneNumbers } },
        { recipientPhoneNumber: { $in: accountsPhoneNumbers } },
      ],
    };

    return dataModel.find(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  getDataModelInstance(data) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataName = sampleData.constructor.name;
    if (!this.dataModel[dataName]) {
      this.dataModel[dataName] = DBUtils.buildModelFrom(data, this.connection);
    }

    return this.dataModel[dataName];
  }
}

exports.TransactionMongoRepo = TransactionMongoRepo;
