const { IRepository } = require('./iRepository');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');

class PaymentAccountMongoRepo extends IRepository {
  constructor(connection) {
    super();
    if (!this.connection) {
      this.connection = connection;
    }
    this.dataModel = {};
  }

  async insert(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    await dataModel.insertMany(dataToSave);
  }

  async findByUuid(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { uuid: data.getUuid() };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async findByPhoneNumber(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { phoneNumber: data.getPhoneNumber() };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async findByUserUuid(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { userUuid: data.getUserUuid() };
    return dataModel.find(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  getDataModelInstance(data) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataName = sampleData.constructor.name;
    if (!this.dataModel[dataName]) {
      this.dataModel[dataName] = DBUtils.buildModelFrom(data, this.connection);
    }

    return this.dataModel[dataName];
  }
}

exports.PaymentAccountMongoRepo = PaymentAccountMongoRepo;
