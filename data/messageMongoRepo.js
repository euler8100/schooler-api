const { IRepository } = require('./iRepository');
const { DBUtils } = require('../utils/dbUtils');
const { Commons } = require('../utils/commons');

class MessageMongoRepo extends IRepository {
  constructor(connection) {
    super();
    if (!this.connection) {
      this.connection = connection;
    }
    this.dataModel = {};
  }

  async insert(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    const members = [data.getSenderUuid(), data.getRecipientUuid()];
    const searchQuery = {
      $or: [
        { senderUuid: members[0], recipientUuid: members[1] },
        { senderUuid: members[1], recipientUuid: members[0] },
      ],
    };
    await dataModel.update(searchQuery, { isLast: false });
    await dataModel.insertMany(dataToSave);
  }

  async insertLast(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    const members = [data.getSenderUuid(), data.getRecipientUuid()];
    const searchQuery = {
      $or: [
        { senderUuid: members[0], recipientUuid: members[1] },
        { senderUuid: members[1], recipientUuid: members[0] },
      ],
    };
    await dataModel.deleteMany(searchQuery);
    await dataModel.insertMany(dataToSave);
  }

  async update(data) {
    const dataModel = this.getDataModelInstance(data);
    const dataToSave = DBUtils.changeToJson(data);
    const searchQuery = { uuid: data.getUuid() };
    await dataModel.updateOne(searchQuery, dataToSave);
  }

  async getLasts(data, userUuid, limit) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = {
      $or: [{ senderUuid: userUuid }, { recipientUuid: userUuid }],
    };
    return dataModel.find(searchQuery, { _id: 0, __v: 0 }).limit(limit).lean();
  }

  async get(data, members) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = {
      $or: [
        { senderUuid: members[0], recipientUuid: members[1] },
        { senderUuid: members[1], recipientUuid: members[0] },
      ],
    };
    return dataModel.find(searchQuery, { _id: 0, __v: 0 }).sort({ sentAt: 1 }).limit(10).lean();
  }

  async delete(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { uuid: data.getUuid() };
    await dataModel.deleteOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  async findOne(data) {
    const dataModel = this.getDataModelInstance(data);
    const searchQuery = { $or: [{ uuid: data.getUuid() }, { pseudo: data.getPseudo() }] };
    return dataModel.findOne(searchQuery, { _id: 0, __v: 0 }).lean();
  }

  getDataModelInstance(data) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataName = sampleData.constructor.name;
    if (!this.dataModel[dataName]) {
      this.dataModel[dataName] = DBUtils.buildModelFrom(data, this.connection);
    }

    return this.dataModel[dataName];
  }
}

exports.MessageMongoRepo = MessageMongoRepo;
