class User {
  constructor(uuid = '') {
    this.uuid = uuid;
    this.fullName = '';
    this.pseudo = '';
    this.password = '';
    this.role = '';
    this.isAccepted = false;
    this.deviceToken = '';
  }

  setUuid(uuid) {
    this.uuid = uuid || this.uuid;
  }

  getUuid() {
    return this.uuid;
  }

  setFullName(fullName) {
    this.fullName = fullName || this.fullName;
  }

  getFullName() {
    return this.fullName;
  }

  setAcceptation(isAccepted) {
    this.isAccepted = isAccepted || this.isAccepted;
  }

  getAcceptation() {
    return this.isAccepted;
  }

  setDeviceToken(deviceToken) {
    this.deviceToken = deviceToken || this.deviceToken;
  }

  getDeviceToken() {
    return this.deviceToken;
  }

  setPseudo(pseudo) {
    this.pseudo = pseudo || this.pseudo;
  }

  getPseudo() {
    return this.pseudo;
  }

  setPassword(password) {
    this.password = password || this.password;
  }

  getPassword() {
    return this.password;
  }

  setRole(role) {
    this.role = role || this.role;
  }

  getRole() {
    return this.role;
  }
}

exports.User = User;
