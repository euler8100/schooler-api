const { signUp } = require('./use-cases/signUp');
const { logIn } = require('./use-cases/logIn');
const { acceptUser } = require('./use-cases/acceptUser');
const { getUsers } = require('./use-cases/getUsers');

class UserController {
  static async signUp(userData, uuid, mongoRepositories, outputPrinter) {
    return signUp(userData, uuid, mongoRepositories, outputPrinter);
  }

  static async logIn(loginData, mongoRepositories, outputPrinter) {
    return logIn(loginData, mongoRepositories, outputPrinter);
  }

  static async acceptUser(acceptationData, mongoRepositories, outputPrinter) {
    return acceptUser(acceptationData, mongoRepositories, outputPrinter);
  }

  static async getUsers(mongoRepositories, outputPrinter) {
    return getUsers(mongoRepositories, outputPrinter);
  }
}

exports.UserController = UserController;
