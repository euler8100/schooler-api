const { Commons } = require('../../utils/commons');
// const { UserOtpController } = require('../../userOtp/controller');
const { User } = require('../user');

async function getUsers(mongoRepositories, outputPrinter) {
  const { userMongoRepo } = mongoRepositories;

  const userSample = new User();
  const users = await userMongoRepo.get(userSample);

  return sendResponse(true, '', users, 'OK', outputPrinter);
}
function sendResponse(requestSuccess, requestMessage, users, responseCode, outputPrinter = null) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {
      users: users.map((user) => buildUserDataFrom(user)),
    },
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

function buildUserDataFrom(user) {
  if (Commons.isEmpty(user)) return {};
  return {
    fullName: user.fullName,
    pseudo: user.pseudo,
    isAccepted: user.isAccepted,
    role: user.role,
    uuid: user.uuid,
    picture: 1,
  };
}

exports.getUsers = getUsers;
