const { Commons } = require('../../utils/commons');
// const { UserOtpController } = require('../../userOtp/controller');
const { User } = require('../user');

async function acceptUser(acceptationData, mongoRepositories, outputPrinter) {
  const { userMongoRepo } = mongoRepositories;
  const validationResponse = await validateAcceptationData(acceptationData, mongoRepositories);
  if (!validationResponse.isValid) {
    return sendResponse(
      false,
      validationResponse.message,
      validationResponse.errorCode,
      outputPrinter,
    );
  }

  const { userUuid } = acceptationData;
  const userSample = new User(userUuid);
  const userData = await userMongoRepo.findOne(userSample);
  if (Commons.isEmpty(userData)) {
    return sendResponse(false, 'Utilisateur inexistant', 'NOT_FOUND', outputPrinter);
  }
  const user = buildUserFrom(userData);
  await setUserAcceptation(user, acceptationData, userMongoRepo);

  return sendResponse(
    true,
    "Status de l'utilisateur modifié avec succès",
    'OK',
    outputPrinter,
    user,
  );
}

async function validateAcceptationData(acceptationData, mongoRepositories) {
  const allPropertiesArePresent = validateAcceptationDataProperties(acceptationData);
  if (!allPropertiesArePresent) {
    return {
      isValid: false,
      message: 'Veuillez entrer tout les parametres requis',
      errorCode: 'BAD_REQUEST',
    };
  }

  return {
    isValid: true,
    message: '',
    errorCode: '',
  };
}

function validateAcceptationDataProperties(acceptationData) {
  if (
    Commons.isEmpty(acceptationData.userUuid)
    || (!acceptationData.hasOwnProperty('isAccepted')
      && (typeof acceptationData.isAccepted).localeCompare('boolean') === 0)
  ) {
    return false;
  }
  return true;
}

function buildUserFrom(userData) {
  const user = new User(userData.uuid);
  user.setFullName(userData.fullName);
  user.setPseudo(userData.pseudo);
  user.setPassword(userData.password);
  user.setDeviceToken(userData.deviceToken);
  user.setAcceptation(userData.isAccepted);
  user.setRole(userData.role);
  return user;
}

async function setUserAcceptation(user, acceptationData, userMongoRepo) {
  if (!acceptationData.isAccepted) {
    await userMongoRepo.delete(user);
    return;
  }
  user.setAcceptation(acceptationData.isAccepted);
  await userMongoRepo.update(user);
}

function sendResponse(
  requestSuccess,
  requestMessage,
  responseCode,
  outputPrinter = null,
  user = {},
) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {
      user: buildUserDataFrom(user),
    },
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

function buildUserDataFrom(user) {
  if (Commons.isEmpty(user)) return {};
  return {
    fullName: user.fullName,
    pseudo: user.pseudo,
    isAccepted: user.isAccepted,
    uuid: user.uuid,
  };
}

exports.acceptUser = acceptUser;
