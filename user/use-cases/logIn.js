const { Commons } = require('../../utils/commons');
// const { UserOtpController } = require('../../userOtp/controller');
const { User } = require('../user');

async function logIn(loginData, mongoRepositories, outputPrinter) {
  const { userMongoRepo } = mongoRepositories;
  const validationResponse = await validateUserData(loginData, mongoRepositories);
  if (!validationResponse.isValid) {
    return sendResponse(
      false,
      validationResponse.message,
      validationResponse.errorCode,
      outputPrinter,
    );
  }

  const { pseudo } = loginData;
  const userSample = new User();
  userSample.setPseudo(pseudo);
  const user = await userMongoRepo.findOne(userSample);
  if (Commons.isEmpty(user)) {
    return sendResponse(false, 'Utilisateur inexistant', 'NOT_FOUND', outputPrinter);
  }
  if (!user.isAccepted) {
    return sendResponse(
      false,
      'Utilisateur non autorisé à la plateforme',
      'UNAUTHORIZED',
      outputPrinter,
    );
  }
  const loginInfoValidation = verifyLoginInfo(loginData, user);
  if (!loginInfoValidation.isValid) {
    return sendResponse(
      false,
      loginInfoValidation.message,
      loginInfoValidation.errorCode,
      outputPrinter,
    );
  }

  return sendResponse(true, 'Utilisateur connecté avec succès', 'OK', outputPrinter, user);
}

async function validateUserData(userData) {
  const allPropertiesArePresent = validateUserDataProperties(userData);
  if (!allPropertiesArePresent) {
    return {
      isValid: false,
      message: 'Veuillez remplire tout les champs requis',
      errorCode: 'BAD_REQUEST',
    };
  }
  return {
    isValid: true,
    message: '',
    errorCode: '',
  };
}

function validateUserDataProperties(userData) {
  if (Commons.isEmpty(userData.pseudo) || !userData.password) {
    return false;
  }
  return true;
}

function verifyLoginInfo(loginData, user) {
  if (loginData.password.localeCompare(user.password) !== 0) {
    return {
      isValid: false,
      message: 'Mot de passe incorrect',
      errorCode: 'UNAUTHORIZED',
    };
  }
  return {
    isValid: true,
    message: '',
    errorCode: '',
  };
}

function sendResponse(
  requestSuccess,
  requestMessage,
  responseCode,
  outputPrinter = null,
  user = {},
) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {
      user: buildUserDataFrom(user),
    },
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

function buildUserDataFrom(user) {
  if (Commons.isEmpty(user)) return {};
  return {
    fullName: user.fullName,
    pseudo: user.pseudo,
    isAccepted: user.isAccepted,
    role: user.role,
    uuid: user.uuid,
    picture: 1,
  };
}

exports.logIn = logIn;
