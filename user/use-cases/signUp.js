const { Commons } = require('../../utils/commons');
const { User } = require('../user');

async function signUp(userData, uuid, mongoRepositories, outputPrinter) {
  const { userMongoRepo } = mongoRepositories;
  const validationResponse = await validateUserData(userData, mongoRepositories);
  if (!validationResponse.isValid) {
    return sendResponse(
      false,
      validationResponse.message,
      validationResponse.errorCode,
      outputPrinter,
    );
  }

  const user = buildUserFrom(userData, uuid);
  await userMongoRepo.insert(user);

  return sendResponse(true, 'Utilisateur créé', 'CREATED', outputPrinter);
}

async function validateUserData(userData, mongoRepositories) {
  const allPropertiesArePresent = validateUserDataProperties(userData);
  if (!allPropertiesArePresent) {
    return {
      isValid: false,
      message: 'Veuillez remplire tout les champs requis',
      errorCode: 'BAD_REQUEST',
    };
  }

  const userAlreadyExists = await verifyUserAlreadyExists(userData, mongoRepositories);
  if (userAlreadyExists) {
    return {
      isValid: false,
      message: "Un utilisateur a déja ce pseudo, prenez en un autre s'il vous plait",
      errorCode: 'CONFLICT',
    };
  }

  return {
    isValid: true,
    message: '',
    errorCode: '',
  };
}

function validateUserDataProperties(userData) {
  if (
    Commons.isEmpty(userData.fullName)
    || Commons.isEmpty(userData.pseudo)
    || Commons.isEmpty(userData.password)
    || Commons.isEmpty(userData.deviceToken)
    || Commons.isEmpty(userData.role)
  ) {
    return false;
  }
  return true;
}

async function verifyUserAlreadyExists(userData, mongoRepositories) {
  const { pseudo } = userData;
  const userSample = new User();
  userSample.setPseudo(pseudo);
  const user = await mongoRepositories.userMongoRepo.findOne(userSample);
  const userExists = !Commons.isEmpty(user);
  return userExists;
}

function buildUserFrom(userData, uuid) {
  const user = new User(uuid.generate());
  user.setFullName(userData.fullName);
  user.setPseudo(userData.pseudo);
  user.setPassword(userData.password);
  user.setRole(userData.role);
  user.setDeviceToken(userData.deviceToken);
  return user;
}

function sendResponse(requestSuccess, requestMessage, responseCode, outputPrinter = null) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {},
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

exports.signUp = signUp;
