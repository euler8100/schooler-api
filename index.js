require('dotenv').config();
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const { Server } = require('./utils/serverUtils');
const { MongoRepositories } = require('./data/mongoRepositories');
const { UuidGenerator } = require('./adapters/uuid');

// Server.initIssuesCatcher();
MongoRepositories.init()
  .then((mongoRepo) => {
    Server.init(mongoRepo, UuidGenerator);
  })
  .catch((error) => {
    Server.handleError(error);
  });
