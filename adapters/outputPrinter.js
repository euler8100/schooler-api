const { IOutputPrinter } = require('./iOutputPrinter');

class OutputPrinter extends IOutputPrinter {
  constructor(printer) {
    super();
    this.printer = printer;
  }

  setPrinter(printer) {
    this.printer = printer;
  }

  setStatusCodeGenerator(statusCodeGenerator) {
    this.statusCodeGenerator = statusCodeGenerator;
  }

  getStatusCodeGenerator() {
    return this.statusCodeGenerator;
  }

  format(msg) {
    this.printer.setHeader('Content-Type', 'application/json');
    return JSON.stringify(msg, null, 2);
  }

  print(msg, statusCode) {
    this.currentStatusCode = statusCode;
    this.format(msg);
    this.printer.status(statusCode).send(msg);
  }

  printError() {
    this.print(
      {
        error: { msg: 'resource not found' },
      },
      this.getStatusCodeGenerator().NOT_FOUND,
    );
  }
}

exports.OutputPrinter = OutputPrinter;
