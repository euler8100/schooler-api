const uuid = require('uuid');
const { IUuid } = require('./iUuid');

class UuidGenerator extends IUuid {
  static generate() {
    return uuid.v4();
  }
}

exports.UuidGenerator = UuidGenerator;
