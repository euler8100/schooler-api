# smallKASH api

**Api of smallKASH app**


## BASE URL


```url
https://apis.chipdeals.me/smallkash
```

## Use

### 1- Create a user

You can create user via a `POST` request to `<BASE_URL>/user/signup`.

You should pass as body a `json object` with following format:
```json
{
  "firstName": "String",
  "lastName": "String",
  "phoneNumber": "Number",
  "deviceToken":"String"
}
```

> Sample request
```http
POST http://localhost:3005/user/signup HTTP/1.1
content-type: application/json

{
  "firstName": "Euler Géraud",
  "lastName":"Dougbe",
  "phoneNumber":22951010580,
  "deviceToken":"user"
}

```
sample response
```json
```
***

### 2- Login

You can log user and get all it's data with this usecase

You should pass as body a `json object` with following format:

```json
{
  "phoneNumber": "Number"
}
```

> Sample request
```http
POST http://localhost:3005/user/login HTTP/1.1
content-type: application/json

{
  "phoneNumber": 22951010580
}


```
sample response
```json
```
***

### 3- Make a transaction

You can log user and get all it's data with this usecase

You should pass as body a `json object` with following format:

```json
{
  "type": "String", // type of transaction: "transfer"||"ask"
  "senderAccountUuid":"String",//Uuid of the account the sender want to use. Required only in transfer request
  "senderPhoneNumber":"Number",//Phone number of the sender. Required only in ask request
  "senderFirstName":"String",//FirstName of the sender. Required only in ask request
  "senderLastName":"String",//LastName of the sender. Required only in ask request
  "recipientAccountUuid":"String",//Uuid of the account on witch asker want to get money. Required only in ask request
  "recipientPhoneNumber":"Number",//Phone number of transfer recipient. Required only in transfer request
  "senderAmount":1, //The amount that should be got from the sender
  "recipientAmount":65,//The amount that should be sent to the recipient
  "fee":"String",//The transaction fee
  "currency":"String"//Currency of the transaction. only "XOF" supported for the moment
}
```

> Sample request of ask
```http
POST http://localhost:3005/transaction/create HTTP/1.1
content-type: application/json
userUuid: d2259ebb-1efb-4dc2-895d-b8fe3e35ce18

{
  "type": "ask",
  "senderPhoneNumber":22951010580,
  "senderFirstName":"firstName",
  "senderLastName":"lastName",
  "recipientAccountUuid":"a027ec91-a0f1-4c84-9451-1308b4a9dcdd",
  "senderAmount":714,
  "recipientAmount":700,
  "fee":"2%",
  "currency":"XOF"  
}
```

> Sample request of transfer
```http
POST http://localhost:3005/transaction/create HTTP/1.1
content-type: application/json
userUuid: 277e6c55-9800-4215-9515-150d6f1a6acf

{
  "type": "transfer",
  "senderAccountUuid":"a027ec91-a0f1-4c84-9451-1308b4a9dcdd",
  "recipientPhoneNumber":22951010580,
  "senderAmount":714,
  "recipientAmount":700,
  "fee":"2%",
  "currency":"XOF"  
}
```

The response is the same for both requests

sample response
```json
```
***

### 4- Answer a transaction of type `ask`


You should pass as body a `json object` with following format:

```json
{
  "transactionUuid":"String",//You get it in the response of the use case 4
  "accepted":true // User acceptation
}
```

> Sample request of ask
```http
POST http://localhost:3005/transaction/answer HTTP/1.1
content-type: application/json

{
  "transactionUuid":"75cbca6e-ada9-4781-8a40-728dee5701fd",
  "accepted":true
}
```

The response is the same for both requests

sample response
```json
```
***



## NOTE

DISCLAIMER: this doc can be updated.

## LICENCE

[ISC](http://opensource.org/licenses/ISC)

Copyright (c) 2021-present, [Chipdeals Inc](https://medium.com/@chipdeals/about).