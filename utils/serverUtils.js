const cors = require('cors');
const Sentry = require('@sentry/node');
const express = require('express');
const statusCodes = require('http-status-codes').StatusCodes;
const expressMonitor = require('express-status-monitor');
const { AuthorizationCheckingUtils } = require('./authorizationCheckingUtils');
const { Commons } = require('./commons');
const { OutputPrinter } = require('../adapters/outputPrinter');
const { UserController } = require('../user/controller');
const { MessageController } = require('../message/controller');

const schooler = express();
schooler.use(Sentry.Handlers.requestHandler());
schooler.use(Sentry.Handlers.errorHandler());
schooler.use(express.json());
schooler.use(expressMonitor(Commons.getMonitorConfig()));
const corsOption = { optionsSuccessStatus: 200 };
corsOption.origin = '*';
schooler.use(cors(corsOption));

Commons.secureMicroservice(schooler);

class Server {
  static initIssuesCatcher() {
    if (Commons.isProductionEnvironment()) {
      Sentry.init({
        dsn: process.env.SENTRY_DSN,
        environment: process.env.NODE_ENV,
        tracesSampleRate: 1.0,
      });
    }
  }

  static async init(mongoRepositories, uuid) {
    Commons.initGlobalVariables();
    schooler.use(async (req, res, next) => {
      res.outputPrinter = new OutputPrinter();
      Commons.ConfigureOutputPrinter(res.outputPrinter, res, statusCodes);

      req.userAuthorization = await AuthorizationCheckingUtils.getAuthorizationDataForm(
        req.headers,
        mongoRepositories,
      );
      console.log('new request with authorization:', req.userAuthorization);

      next();
    });

    schooler.post('/user/signUp', (req, res) => {
      UserController.signUp(req.body, uuid, mongoRepositories, res.outputPrinter);
    });

    schooler.post('/user/accept', (req, res) => {
      if (!req.userAuthorization.success) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas enregistré(e)",
          data: {},
        });
        return;
      }
      if (!req.userAuthorization.user.role.startsWith('admin')) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas autorisé(e) à accepter un utilisateur",
          data: {},
        });
        return;
      }
      UserController.acceptUser(req.body, mongoRepositories, res.outputPrinter);
    });

    schooler.get('/user', (req, res) => {
      if (!req.userAuthorization.success) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas enrégistré(e)",
          data: {},
        });
        return;
      }
      UserController.getUsers(mongoRepositories, res.outputPrinter);
    });

    schooler.post('/user/login', (req, res) => {
      UserController.logIn(req.body, mongoRepositories, res.outputPrinter);
    });

    schooler.post('/message/send', (req, res) => {
      if (!req.userAuthorization.success) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas enrégistré(e)",
          data: {},
        });
        return;
      }
      const messageData = { ...req.body, senderUuid: req.userAuthorization.userUuid };
      MessageController.sendMessage(messageData, uuid, mongoRepositories, res.outputPrinter);
    });

    schooler.get('/message/last/:messageLimit', (req, res) => {
      if (!req.userAuthorization.success) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas enrégistré(e)",
          data: {},
        });
        return;
      }

      MessageController.getLasts(
        req.userAuthorization.userUuid,
        req.params.messageLimit,
        mongoRepositories,
        res.outputPrinter,
      );
    });

    schooler.get('/message/:bynomeUuid', (req, res) => {
      if (!req.userAuthorization.success) {
        res.status(statusCodes.UNAUTHORIZED).send({
          success: false,
          message: "Vous n'êtes pas enrégistré(e)",
          data: {},
        });
        return;
      }
      const { bynomeUuid } = req.params;
      const { userUuid } = req.userAuthorization;
      MessageController.getMessages(userUuid, bynomeUuid, mongoRepositories, res.outputPrinter);
    });

    schooler.all('*', ({ res }) => {
      res.status(statusCodes.BAD_REQUEST).send({ error: { msg: 'Bad Request' } });
    });

    const microserviceServer = schooler.listen(process.env.PORT);
    process.on('SIGTERM', () => {
      microserviceServer.close(() => {
        console.log('Http server closed.');
      });
    });
  }

  static handleError(serverBootingError) {
    Commons.catchError(serverBootingError);
    throw new Error(serverBootingError);
  }
}

exports.Server = Server;
