const { Commons } = require('./commons');

class DBUtils {
  static getDBConnectionOptions() {
    return {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
      keepAlive: true,
      keepAliveInitialDelay: 300000,
    };
  }

  static buildModelFrom(data, dbConnection) {
    const sampleData = Commons.getSingleElementFrom(data);
    const dataSchema = {};
    Object.keys(sampleData).map((key) => (dataSchema[key] = typeof sampleData[key]));

    return dbConnection.model(sampleData.constructor.name, dataSchema);
  }

  static async replaceUuid(dataModel, dataToSave, fieldToCompare = ['name']) {
    const registeredData = await dataModel.find({});
    if (Commons.isEmpty(registeredData)) {
      return;
    }
    Object.values(dataToSave).forEach((value) => {
      registeredData
        .filter((data) => {
          const areSimilarData = fieldToCompare.reduce(
            (areSimilar, field) => areSimilar && data[field] == value[field],
            true,
          );
          return areSimilarData;
        })
        .map((data) => (value.uuid = data.uuid));
    });
  }

  static changeToJson(data) {
    if (Array.isArray(data)) {
      return JSON.parse(JSON.stringify(data.flat()));
    }
    return JSON.parse(JSON.stringify(data));
  }

  static async updateMany(dataModel, dataToSave, deletionCriteria = {}) {
    await dataModel.deleteMany(deletionCriteria);
    await dataModel.insertMany(dataToSave);
  }

  static getSupportedCinemaCompanies() {
    return [{ name: 'canalOlympia', url: 'https://www.canalolympia.com/' }];
  }
}

exports.DBUtils = DBUtils;
