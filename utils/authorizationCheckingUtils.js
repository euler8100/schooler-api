const { Commons } = require('./commons');
const { User } = require('../user/user');

class AuthorizationCheckingUtils {
  static async getAuthorizationDataForm(requestHeaders, mongoRepositories) {
    const { useruuid: userUuid } = requestHeaders;
    if (Commons.isEmpty(userUuid)) return { success: false, userUuid: null };

    const { userFound: isValidUserUuid, user } = await verifyUserUuid(userUuid, mongoRepositories);
    return { userUuid, success: isValidUserUuid, user };
  }
}

async function verifyUserUuid(userUuid, mongoRepositories) {
  const userSample = new User(userUuid);
  const registeredUser = await mongoRepositories.userMongoRepo.findOne(userSample);
  return { userFound: !Commons.isEmpty(registeredUser), user: registeredUser || {} };
}

exports.AuthorizationCheckingUtils = AuthorizationCheckingUtils;
