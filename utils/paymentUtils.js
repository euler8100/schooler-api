const { RequestUtils } = require('./requestUtils');
const { Commons } = require('./commons');

class PaymentUtils {
  static async transfer(transaction, narration) {
    const requestData = getTransferDataFrom(transaction);
    console.log(requestData);
    const transferResponse = await RequestUtils.sendRequestTo(
      process.env.TRANSFER_URL,
      requestData,
      'post',
    );
    return parseResponse(transferResponse);
  }

  static async getStatus(transferReference) {
    const response = await RequestUtils.getDataFrom(
      `${process.env.TRANSFER_URL}/${transferReference}`,
    );
    return parseResponse(response);
  }

  static parseTransferResponse(transferResponse) {
    return parseResponse(transferResponse);
  }
}

function getTransferDataFrom(transaction) {
  return {
    webhookUrl: `${process.env.BASE_URL}/paymentWebhook`,
    sender: {
      narration: "envoi d'argent",
      userInfo: {
        countryCode: transaction.getSenderCountryCode(),
        firstName: transaction.getSenderFirstName(),
        lastName: transaction.getSenderLastName(),
        phoneNumber: transaction.getSenderPhoneNumber(),
      },
      price: {
        amount: transaction.getSenderPriceAmount(),
        currency: transaction.getPriceCurrency(),
      },
    },
    recipient: {
      userInfo: {
        countryCode: transaction.getRecipientCountryCode(),
        phoneNumber: transaction.getRecipientPhoneNumber(),
      },
      price: {
        amount: transaction.getRecipientPriceAmount(),
        currency: transaction.getPriceCurrency(),
      },
    },
  };
}

function parseResponse(transferResponse) {
  console.log(transferResponse);
  if (!Commons.isEmpty(transferResponse.error)) {
    return {
      transferSuccess: false,
      transferFailure: true,
      transferReference: '',
      transferStatus: 'error',
      message: transferResponse.error.msg,
    };
  }
  const parsedResponse = {
    transferSuccess: transferResponse.transfer.status.localeCompare('success') === 0,
    transferFailure: transferResponse.transfer.status.localeCompare('error') === 0,
    transferReference: transferResponse.transfer.reference,
    transferStatus: transferResponse.transfer.status,
    message: '',
  };
  setResponseMessage(parsedResponse, transferResponse);
  return parsedResponse;
}

function setResponseMessage(parsedResponse, transferResponse) {
  let message = 'pending';
  if (parsedResponse.transferSuccess) message = 'transfer successful';
  if (parsedResponse.transferFailure) {
    message = transferResponse.transfer.depositResponse.message;
    if (transferResponse.transfer.paymentStatus.localeCompare('error') === 0) {
      message = transferResponse.transfer.paymentResponse.message;
    }
  }
  parsedResponse.message = message;
}

exports.PaymentUtils = PaymentUtils;
