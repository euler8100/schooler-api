const hpp = require('hpp');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const xss = require('xss-clean');
const express = require('express');
const bodyParser = require('body-parser');
const mongoSanitize = require('express-mongo-sanitize');
const isOnline = require('is-online');
const Sentry = require('@sentry/node');

class Commons {
  static initGlobalVariables() {
    global.totalCurrentAxiosRequests = 0;
    global.currentAxiosRequestUrls = [];

    global.isConnected = false;
    checkConnection();
  }

  static isProductionEnvironment = () => process.env.NODE_ENV === 'prod';

  static getMonitorConfig() {
    const projectName = Commons.parseLocalJSONFile('package.json', '').name;
    const projectVersion = Commons.parseLocalJSONFile('package.json', '').version;
    const monitorConfig = {
      title: projectName + ' V- ' + projectVersion,
      theme: 'default.css',
      path: process.env.MONITOR_CONFIG_PATH
    };
    monitorConfig.socketPath = process.env.MONITOR_SOCKET_PATH;
    return monitorConfig;
  }

  static catchError(data) {
    if (Commons.isProductionEnvironment()) {
      Sentry.captureException(data);
    } else {
      console.error(data);
    }
  }

  static catchLog(data) {
    if (Commons.isProductionEnvironment()) {
      Sentry.captureMessage(data);
    } else {
      console.log(data);
    }
  }

  static getSingleElementFrom(data) {
    if (Array.isArray(data)) {
      const [firstElement] = data.flat();
      return firstElement;
    }
    return data;
  }

  static isEmpty(data) {
    if (!data) {
      return true;
    }
    if (Array.isArray(data)) {
      return data.length === 0;
    }
    const dataProperties = Object.values(data);
    return dataProperties && dataProperties.length == 0;
  }

  static anyIsEmpty() {
    let anyIsEmpty = false;
    [...arguments]
      .map((singleData) => {
        return Commons.isEmpty(singleData);
      })
      .map((singleDataIsEmpty) => (anyIsEmpty = anyIsEmpty || singleDataIsEmpty));
    return anyIsEmpty;
  }

  static ConfigureOutputPrinter(outputPrinter, printer, statusCode) {
    if (outputPrinter) {
      outputPrinter.setPrinter(printer);
      outputPrinter.setStatusCodeGenerator(statusCode);
    }
  }

  static secureMicroservice(microservice) {
    microservice.disable('x-powered-by');
    microservice.use(helmet());
    microservice.use(express.json({ limit: '10kb' })); // Body limit is 10
    microservice.use(xss());
    microservice.use(mongoSanitize());
    microservice.use(bodyParser.urlencoded({ extended: true, parameterLimit: 3 }));
    microservice.use(hpp());
  }

  static parseLocalJSONFile(fileName, filePath = '/raws') {
    const rawData = fs.readFileSync(path.join(__dirname, '..', filePath, fileName));
    return JSON.parse(rawData);
  }
}
function checkConnection() {
  setTimeout(async () => {
    global.isConnected = await isOnline();
    if (!global.isConnected) console.log('You are not connected to the Internet');
    checkConnection();
  }, 5000);
}

exports.Commons = Commons;
