const { RequestUtils } = require('./requestUtils');
const { Commons } = require('./commons');

class NotificationUtils {
  static async sendDataToUser(deviceToken, data) {
    if (Commons.isEmpty(deviceToken)) return;
    const requestData = { devicesTokens: [deviceToken], data, isDataNotification: true };
    await RequestUtils.sendRequestTo(process.env.NOTIFICATION_API_URL, requestData, 'post');
  }
}

exports.NotificationUtils = NotificationUtils;
