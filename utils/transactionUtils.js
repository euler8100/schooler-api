const { Transaction } = require('../transaction/transaction');
const { RequestUtils } = require('./requestUtils');
const { PaymentUtils } = require('./paymentUtils');
const { Commons } = require('./commons');

class TransactionUtils {
  static buildTransactionFrom(transactionData, senderAccount = {}, recipientAccount = {}) {
    const trD = transactionData;
    const transaction = new Transaction(trD.uuid);
    transaction.setSenderPhoneNumber(senderAccount.phoneNumber || trD.senderPhoneNumber);
    transaction.setSenderFirstName(senderAccount.firstName || trD.senderFirstName);
    transaction.setSenderLastName(senderAccount.lastName || trD.senderLastName);
    transaction.setSenderCountryCode(senderAccount.countryCode || trD.senderCountryCode);
    transaction.setRecipientPhoneNumber(recipientAccount.phoneNumber || trD.recipientPhoneNumber);
    transaction.setRecipientFirstName(recipientAccount.firstName || trD.recipientFirstName);
    transaction.setRecipientLastName(recipientAccount.lastName || trD.recipientLastName);
    transaction.setRecipientCountryCode(recipientAccount.countryCode || trD.recipientCountryCode);
    transaction.setPriceCurrency(trD.priceCurrency);
    transaction.setSenderPriceAmount(trD.senderPriceAmount);
    transaction.setRecipientPriceAmount(trD.recipientPriceAmount);
    transaction.setTransactionStatus(trD.transactionStatus);
    transaction.setPaymentApiReference(trD.paymentApiReference);
    transaction.setTransactionResponseMessage(trD.transactionResponseMessage);
    transaction.setTransactionMessage(trD.transactionMessage);
    transaction.setTransactionStartedAt(trD.transactionStartedAt);
    transaction.setTransactionEndedAt(trD.transactionEndedAt);
    transaction.setPaymentLink(trD.paymentLink);
    transaction.setFee(trD.fee);
    transaction.setType(trD.type);
    return transaction;
  }

  static buildTransactionResponseFrom(transaction) {
    let transactionObject = transaction;
    if (Commons.isEmpty(transactionObject.getUuid)) {
      transactionObject = TransactionUtils.buildTransactionFrom(transaction);
    }
    return {
      uuid: transactionObject.getUuid(),
      senderPhoneNumber: transactionObject.getSenderPhoneNumber(),
      senderFirstName: transactionObject.getSenderFirstName(),
      senderLastName: transactionObject.getSenderLastName(),
      recipientPhoneNumber: transactionObject.getRecipientPhoneNumber(),
      recipientFirstName: transactionObject.getRecipientFirstName(),
      recipientLastName: transactionObject.getRecipientLastName(),
      priceCurrency: transactionObject.getPriceCurrency(),
      senderPriceAmount: transactionObject.getSenderPriceAmount(),
      recipientPriceAmount: transactionObject.getRecipientPriceAmount(),
      transactionStatus: transactionObject.getTransactionStatus(),
      transactionResponseMessage: transactionObject.getTransactionResponseMessage(),
      transactionMessage: transactionObject.getTransactionMessage(),
      transactionStartedAt: transactionObject.getTransactionStartedAt(),
      transactionEndedAt: transactionObject.getTransactionEndedAt(),
      paymentLink: transactionObject.getPaymentLink(),
      fee: transactionObject.getFee(),
      type: transactionObject.getType(),
    };
  }

  static async transferMoney(transaction, mongoRepositories) {
    const paymentResponse = await PaymentUtils.transfer(transaction);
    console.log(paymentResponse);
    await updateTransactionResponse(paymentResponse, mongoRepositories, transaction);
    await notifyClients(transaction);
  }

  static async completeTransaction(paymentWebhookData, mongoRepositories) {
    const { transactionMongoRepo } = mongoRepositories;
    const paymentResponse = PaymentUtils.parseTransferResponse(paymentWebhookData);

    const transactionSample = new Transaction();
    transactionSample.setPaymentApiReference(paymentResponse.transferReference);
    const transactionData = await transactionMongoRepo.findByPaymentReference(transactionSample);
    if (Commons.isEmpty(transactionData)) return;
    const transaction = TransactionUtils.buildTransactionFrom(transactionData);

    await updateTransactionResponse(paymentResponse, mongoRepositories, transaction);
    await notifyClients(transaction);
  }
}

async function updateTransactionResponse(paymentResponse, mongoRepositories, transaction) {
  const { transactionMongoRepo } = mongoRepositories;
  transaction.setPaymentApiReference(paymentResponse.transferReference);
  transaction.setTransactionResponseMessage(paymentResponse.message);
  transaction.setTransactionStatus(paymentResponse.message);
  await transactionMongoRepo.update(transaction);
}

async function notifyClients(transaction) {
  console.log("TODO: update function 'notifyClients' of TransactionUtils", transaction);
}

exports.TransactionUtils = TransactionUtils;
