const { RequestUtils } = require('./requestUtils');
const { Commons } = require('./commons');

class LinkUtils {
  static async buildLinkFrom(dataToEncode) {
    const requestData = {
      branch_key: process.env.BRANCH_IO_KEY,
      data: {
        $og_title: 'Paiement de via smallKASH',
        // "$og_description": "Description from Deep Link",
        // "$og_image_url": "http://www.lorempixel.com/400/400/",
        // url: process.env.SMALLKASH_PAYMENT_PAGE,
        ...dataToEncode,
      },
    };
    const linkRequestResponse = await RequestUtils.sendRequestTo(
      process.env.BRANCH_IO_LINK_GENERATION,
      requestData,
      'post',
    );
    console.log(requestData);
    console.log(linkRequestResponse);
    return linkRequestResponse.url;
  }
}

exports.LinkUtils = LinkUtils;
