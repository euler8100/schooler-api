class Message {
  constructor(uuid = '') {
    this.uuid = uuid;
    this.sentAt = new Date().getTime();
    this.senderUuid = '';
    this.recipientUuid = '';
    this.message = '';
    this.seen = false;
    this.isLast = true;
  }

  setUuid(uuid) {
    this.uuid = uuid || this.uuid;
  }

  getUuid() {
    return this.uuid;
  }

  setSentAt(sentAt) {
    this.sentAt = sentAt || this.sentAt;
  }

  getSentAt() {
    return this.sentAt;
  }

  setSenderUuid(senderUuid) {
    this.senderUuid = senderUuid || this.senderUuid;
  }

  getSenderUuid() {
    return this.senderUuid;
  }

  setRecipientUuid(recipientUuid) {
    this.recipientUuid = recipientUuid || this.recipientUuid;
  }

  getRecipientUuid() {
    return this.recipientUuid;
  }

  setMessage(message) {
    this.message = message || this.message;
  }

  getMessage() {
    return this.message;
  }

  setSeenStatus(seen) {
    this.seen = seen || this.seen;
  }

  getSeenStatus() {
    return this.seen;
  }
}

exports.Message = Message;
