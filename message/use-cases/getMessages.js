const { Commons } = require('../../utils/commons');
const { Message } = require('../message');

async function getMessages(userUuid, binomeUuid, mongoRepositories, outputPrinter) {
  const { messageMongoRepo } = mongoRepositories;

  const messageSample = new Message();
  const messages = await messageMongoRepo.get(messageSample, [userUuid, binomeUuid]);

  return sendResponse(true, '', messages, 'OK', outputPrinter);
}
function sendResponse(
  requestSuccess,
  requestMessage,
  messages,
  responseCode,
  outputPrinter = null,
) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {
      messages,
    },
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

exports.getMessages = getMessages;
