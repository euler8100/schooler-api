const { Commons } = require('../../utils/commons');
// const { UserOtpController } = require('../../userOtp/controller');
const { LastMessage } = require('../lastMessage');

async function getLasts(userUuid, messageLimit, mongoRepositories, outputPrinter) {
  const { messageMongoRepo } = mongoRepositories;

  const lastMessageSample = new LastMessage();
  const lastMessages = await messageMongoRepo.getLasts(
    lastMessageSample,
    userUuid,
    parseInt(messageLimit),
  );

  return sendResponse(true, '', userUuid, lastMessages, 'OK', outputPrinter);
}

function sendResponse(
  requestSuccess,
  requestMessage,
  userUuid,
  lastMessages,
  responseCode,
  outputPrinter = null,
) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {
      lastsMessages: lastMessages.map((lastMessage) => buildConversationMessage(lastMessage, userUuid)),
    },
  };

  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

function buildConversationMessage(message, userUuid) {
  let conversationBinomeUuid = message.senderUuid;
  if (userUuid.localeCompare(conversationBinomeUuid) === 0) {
    conversationBinomeUuid = message.recipientUuid;
  }
  return {
    conversationBinomeUuid,
    message,
  };
}

exports.getLasts = getLasts;
