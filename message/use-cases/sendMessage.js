const { Commons } = require('../../utils/commons');
const { Message } = require('../message');
const { LastMessage } = require('../lastMessage');

async function sendMessage(messageData, uuid, mongoRepositories, outputPrinter) {
  const { messageMongoRepo } = mongoRepositories;

  const message = buildMessage(messageData, uuid);
  const lastMessage = buildLastMessage(message);
  await messageMongoRepo.insert(message);
  await messageMongoRepo.insertLast(lastMessage);

  return sendResponse(true, 'Message envoyé', 'OK', outputPrinter);
}

function buildMessage(messageData, uuid) {
  const message = new Message(uuid.generate());
  message.setSenderUuid(messageData.senderUuid);
  message.setRecipientUuid(messageData.recipientUuid);
  message.setMessage(messageData.message);
  return message;
}

function buildLastMessage(message) {
  const lastMessage = new LastMessage(message.getUuid());
  lastMessage.setSenderUuid(message.getSenderUuid());
  lastMessage.setRecipientUuid(message.getRecipientUuid());
  lastMessage.setMessage(message.getMessage());
  return lastMessage;
}

function sendResponse(requestSuccess, requestMessage, responseCode, outputPrinter = null) {
  const response = {
    success: requestSuccess,
    message: requestMessage,
    data: {},
  };
  if (!Commons.isEmpty(outputPrinter)) {
    const statusCodes = outputPrinter.getStatusCodeGenerator();
    outputPrinter.print(response, statusCodes[responseCode]);
  }
  return response;
}

exports.sendMessage = sendMessage;
