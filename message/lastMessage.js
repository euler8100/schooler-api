const { Message } = require('./message');

class LastMessage extends Message {
  constructor(uuid) {
    super(uuid);
  }
}

exports.LastMessage = LastMessage;
