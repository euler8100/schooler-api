const { getLasts } = require('./use-cases/getLasts');
const { getMessages } = require('./use-cases/getMessages');
const { sendMessage } = require('./use-cases/sendMessage');

class MessageController {
  static async getLasts(userUuid, messageLimit, mongoRepositories, outputPrinter) {
    return getLasts(userUuid, messageLimit, mongoRepositories, outputPrinter);
  }

  static async getMessages(userUuid, binomeUuid, mongoRepositories, outputPrinter) {
    return getMessages(userUuid, binomeUuid, mongoRepositories, outputPrinter);
  }

  static async sendMessage(messageData, uuid, mongoRepositories, outputPrinter) {
    return sendMessage(messageData, uuid, mongoRepositories, outputPrinter);
  }
}

exports.MessageController = MessageController;
